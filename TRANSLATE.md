# Translation of documents in this repo

## Translate

Create an account at https://translate.zanata.org/account/register.

Then go to https://translate.zanata.org/project/view/apoderamentdigital.

## Keep translations in sync (repo admin)

Prerequisites:

* [po4a](https://po4a.org/download.php)
* [Zanata CLI](http://docs.zanata.org/en/latest/client/)

The following commands use document `la_guia/ESTRATEGIES.md` as example and `es` (spanish) as the language to translate to (from our master in catalan).

To create a template from scratch, do:

    po4a-gettextize -m la_guia/ESTRATEGIES.md -f text -o markdown -o neverwrap -M utf8 -p po_files/es/ESTRATEGIES.po
    zanata-cli push -l es --push-type both

After modifying the spanish version online in Zanata, do:

    zanata-cli pull -l es
    po4a-translate  -m la_guia/ESTRATEGIES.md -f text -o markdown -o neverwrap -M utf8 -p po_files/es/ESTRATEGIES.po -l locale/es/ESTRATEGIES.md -k 0 -w 10000

After modifying the catalan version (master) offline, do:

    po4a-updatepo   -m la_guia/ESTRATEGIES.md -f text -o markdown -o neverwrap -M utf8 -p po_files/es/ESTRATEGIES.po
    po4a-updatepo   -m la_guia/ESTRATEGIES.md -f text -o markdown -o neverwrap -M utf8 -p po_files/en/ESTRATEGIES.po
    po4a-updatepo   -m la_guia/ESTRATEGIES.md -f text -o markdown -o neverwrap -M utf8 -p po_files/pot/ESTRATEGIES.pot
    zanata-cli push -l es --push-type both
