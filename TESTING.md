# Entorn de prova de la web
La manera més fàcil de crear un entorn de prova local és fent servir docker:

```
docker run --rm --volume=$PWD:/srv/jekyll -p 4000:4000 -it jekyll/jekyll:3.8 bash
```

Un cop dins cal executar:
```
jekyll serve -s web
```

Llavors la pàgina hauria de ser accessible a http://0.0.0.0:4000/.
Val la pena mirar els errors que de vegades detecta per poder-los arreglar.

# Inclou el contingut!
Ara mateix tenim el contingut a `la_guia/` però la web es genera exclusivament en base al que hi ha a `web/`. Cal copiar alguns fitxers, una bona manera és mirar el firxer `.gitlab-ci.yml`.
