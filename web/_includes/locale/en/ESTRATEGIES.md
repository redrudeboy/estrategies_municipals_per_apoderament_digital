## Measures for digital empowerment in municipalities

### Telecommunications infrastructure

The telecommunications infrastructure is all that wiring and hardware that makes it possible for information to travel from one place to another.
It is the pillar of the Internet.
Currently we have public, private infrastructure and commons, according to which is its owner and which governance model it has.

The decisions made regarding the regulation, development and expansion of this infrastructure have been the subject of strong tensions between different private interests seeking to monitor the network and the information that circulates in order to extract a profit.
We have also been able to see how the control of this network has been used in campaigns of censorship.


#### General objectives

* Tend towards the universalization and sovereignty of telecommunication
  networks, enhancing the neutral use of telecommunication networks.

* Promoting the deployment of fixed and mobile telecommunications services,
  existing and new, such as the 5G, is made through a network shared by
  different operators and public and private users, preventing the
  privatization of public space without sharing.

* Promote local operators and cooperative in nature, avoiding the monopoly
  of big operators, increasing their control.

* Deploy and consolidate an Internet of things data network that is open,
  free and neutral, created collectively as a grassroots initatives.


#### Programmatic proposal

##### Consolidation of an ecosystem of collaborative and open infrastructure of free technologies

1. Provide real access for citizens and society in general to an affordable
   and varied offer of telecommunications services of the highest quality,
   capacity and network neutrality.

2. Promote municipal ordinances for the deployment of new infrastructure as
   proposed by guifi.net, which promotes with the maximum agility and
   efficiency, stimulates and maximizes the efficiency of any type of
   investment and, at the same time ensures their sustainability on the
   basis of the use made, minimizing the cost to the public administration
   and also for the citizens and society in general.

3. Encourage open community networks, focusing on the need for evaluation of
   social and territorial impact of deployments, taking into account the
   possible use cases.

##### Deployment of new infrastructures for communication services (existing or new generation such as 5G and future)

4. Promotion of the deployment of access infrastructures (fixed and mobile,
   existing or new such as 5G) through a network shared by the different
   operators.

5. Facilitate the sharing of public infrastructure: deployment of antennas
   or passing of wiring in buildings and public infrastructure.

##### Procurement of telecommunications services

6. Hire services to telecommunications companies that would not
   discriminate, filter or interrupt traffic, as in the case of the
   self-determination referendum of the 1 October 2017.

##### The Internet of Things

7. Promote initiatives for internet of things networks that are open, free
   and neutral.

8. Contribute to an architecture of the Internet of things for cities to be
   open and interoperable.


### Free Software

When a computer program you can be used, modified, shared and studied, we call it free.
Free software allows us to resolve common computer needs in an inclusive manner, in which different actors can have proactive interest in a solution without having to develop it twice or ask a third party to do it for us.
It prevents the dependence on third parties who have a grip on the software and makes it possible to verify what exactly the program does, both to ensure that it does what it should do, as well as to ensure that it is not processing our data in unwanted ways.

#### General objectives

* Achieve full municipal sovereignty in respect of what code and algorithms are run, what data is stored and who has access.
Make the code auditable by the citizens.

* Decouple municipalities from the commercial strategies of providers,
  improving the ability of decision about what technologies are deployed
  promoting at the same time innovation and strengthening the local
  technology sector.

* Get the best software solutions adapted to each case and easier to evolve,
  at a lower cost.

* Contribute to the sustainability of the digital asset which is free
  software.

#### Programmatic proposal

##### Relationship with citizens

9. Ensure that all the websites and municipal applications work without any
   disadvantage with free software.

10. Only use open formats and protocols, with public specifications and that
    have good free implementations.

11. Do not add to the municipal web sites third-party services based on proprietary software, or that allow unnecessary data collection by the service provider.
In general, do not distribute proprietary software in any way.

12. Do not accept donations of proprietary software from private companies, nor of hardware that requires the use of proprietary software for proper operation.
This point is particularly important in the field of education.

##### Deployment of free software in every municipality

13. Establish a rigorous plan of migration to free software for all
    information services of the city council.

14. Migrate internal office software to free software, with the relevant
    training programmes.

15. That with public money will only be used to develop public code: make sure in the new contracts that code is published under free licenses.
Design integration with proprietary components so that they are temporary and easily replaceable.

16. Deploy the essential services on the computers of public servants to at
    least have online access.

17. Do not buy hardware that is not fully functional under free kernels,
    such as Linux.

18. Progressively migrate to free software all desktop applications and the
    operating system of municipal computers.

##### Cooperation between administrations and with free software communities

19. Create coordination mechanisms among administrations that will allow
    sharing the development and deployment of services.

20. Prioritise participation in existing projects, making sure that the
    improvements made are included in the original product. Respect the
    practices and codes of conduct of the communities in which it
    participates.

21. Encourage the re-use of solutions by means of the correct documentation
    and communication of projects.


### Data policy

That knowledge is power is something always known.
Today, using data we have a lot of possibility of obtain knowledge.
We will need to find ways to protect data, respecting the privacy of citizens.
For this reason, we need to ensure public access to open data and tools to avoid private monopolies of data.

#### General objective of the field

* Ensure a public administration that is transparent and close to citizens.

* Facilitate the access to and exploitation of public data sets.

* Expand open data sets.

* Ensure, with ethical criteria, the privacy of the personal data,
  administering them carefully, and reducing the risks arising from their
  use.


#### Programmatic proposal

##### Open data by default

22. Publish under free licenses and reusable formats all public information
    created or maintained by the local administration (from municipal
    budgets up to timing of traffic lights, for example).

23. Standardize the formats of the data published between different
    municipalities and administrations, thus facilitating cross-analysis of
    data.

24. Publish the raw data generated for the implementation of studies funded
    by public money, to allow checking out the studies or the development of
    derivatives.

25. Promote the reuse of this data, via active announcement, good
    accessibility to the municipal site, and the specification of licenses.

##### Personal data privacy

26. Manage personal data only within the administration and in accordance
    with strict safety criteria, ensuring the technical means and expertise
    to manage them without relying on third parties.

27. Limit the collection of personal data to those cases where the specific
    purposes for which it will be used were previously established, thus
    minimizing the amount of personal data that is collected.

28. Implement mechanisms to give a maximum of services to know, correct or
    delete personal data from public systems; and not just the legal minimum
    required by the GDPR.

### Democratization of technology

The easy access of citizens to technology, especially relating to Internet connectivity and the use of smart phones, involves a transformation of our society towards a new digital culture.
The democratization of technology, in terms of cost reduction and ease of connectivity, allow the creation of new public services adapted to the needs of their citizens and accessible from your phone without face-to-face presence or paperwork.
This digital culture must permeate the generation of services to the citizens, since they mean a cost reduction of service provision and facilitate access.

The public administration should ensure the equality of access to these new services, providing tools and resources that guarantee their use and the benefits, regardless of socio-economic background, gender or different capabilities of citizens.


#### General objective of the field

* Put at the disposal of everyone the technological innovations that the
  public administration incorporates in its municipal action, safeguarding a
  non-discriminatory access to procedures, digital services and actions of
  the public administration.

* Work to decrease the distance created from different technological uses from the part of the different sectors of society (digital divide), created both for access to technology as well as knowledge.
Accompany all municipal action that includes a digital aspect with the necessary training.

* Promote training and educational policies that promote to highlight gender
  diversity in the technology world as well as the promotion technological
  orientations in girls.

* Promote a conscious and critical relationship of citizens with technology.

#### Programmatic proposal

29. Set up training programs and capacity building about the digital for
    citizenship, as well as strengthen existing ones.

30. Promote spaces of connection and technology use that offer free of
    charge tools and resources, as well as provide resources to existing
    ones (libraries, community centres or telecentres).

31. Provide educational programs in coordination with educational centres at
    different levels that allow address how to address technology in an
    inclusive and respectful manner with citizens, introducing a gender
    perspective.

32. Offer technology training (initial and advanced) to groups at risk of
    social exclusion for socio-economic reasons, on topics on technological
    innovation, in coordination with the municipal social services.

33. Ensure that public administration staff can support citizens in their
    digital interaction with the public administration on the basis of
    internal training programs.

34. Ensure accessibility to people with disabilities.
It is necessary that the formats of representation of information meet the standards and recommendations regarding the representation of information for people with different abilities.

35. Need to ensure that the models and algorithms, especially in the context
    of automatic learning, do not introduce bias that harm people, such as
    gender, race, religion or sexual orientation.

### Public procurement of electronic devices and circularity

The time of determining the conditions of public procurement of an electronic device is when there is the possibility of influencing the conditions of manufacture of the product but also in determining the conditions for its reuse once no longer used, to exhaust all its usable life, and ensuring the final recycling (circular economy).

#### General objective of the field

* Procurement of electronic devices that minimize the negative impact on
  people and the environment.

#### Programmatic proposal

36. Promotion of reduction of environmental impact and the social and
    solidarity economy.

37. Respect labour rights of workers involved in the manufacture,
    maintenance, collection and recycling of electronic devices.

38. Responsible public procurement that respect labor rights in the
    production of electronic goods.

39. Traceability of the origin of the devices to ensure circularity: repair,
    renew, reuse, ensure final recycling and the transparency of the data
    about these processes.

40. Commitment to donation of the devices to social entities at the end of
    its use, to extend the lifetime of the devices and create social
    benefits with reuse.

41. Public procurement with extension of warranty that includes repair and
    maintenance throughout the whole period of use, to help extend the
    usable life of the devices.

42. Promotion of knowledge about maintenance and repair of the devices for
    the public entities and education centres (schools, universities).


### Free standards

The documents, data and interaction between people and municipal services are a necessity and a resource for the general public in many aspects of his life.
The format and the tools of access should never be an obstacle.
The specifications and free formats are fully documented and are publicly available; they must be free of copyright restrictions, intellectual property claims or restrictive licences; the format is decided by a provider independent standards organization or a community (e.g., open source development community); there are implementations of free use support tools.

#### General objectives

* Facilitate interoperability and data sharing in the services funded with
  public funds for the benefit of the citizens, avoiding barriers to access,
  using specifications for data formats and services that follow open
  standards.

* Avoid creating dependencies and third-party costs: preference for
  standards of data formats and protocols free of patents, licenses and
  intellectual rights.

* Avoid conditioning decisions with preference for free protocols, widely
  implemented and applied in a variety of products and services.

#### Programmatic proposal

43. Ensure interoperability: use open file formats as a tool to ensure access to information without obstacles and good interaction with and among public systems.
In addition, the standards that are used must have open implementations that is verified to work with a variety of devices or software, always including a free software option.

44. One of the requirements in the procurement of technological equipment must be the use of free standards to perform its function.
This way eliminates dependencies on a single manufacturer.

45. Digital production using copyleft licenses and Creative Commons licences
    that facilitate reuse of artistic works and productions of local
    creators funded publicly, as tools of free culture.

46. Set active policies of use of open standards and free software
    migration, having established a list of applications approved for each
    use and functionality required by the public sector, and enable reuse
    strategies, collaboration and sharing of efforts with other public
    entities around the world.
